const path = require('path');
const webpack = require('webpack');

const config = require('./config');

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlPlugin = require('html-webpack-plugin');

const htmlPlugin = new HtmlPlugin({
  template: './config/index.template.html',
  title: config.title,
  inject: 'body'
});
const commonsChunk = new webpack.optimize.CommonsChunkPlugin({
  name: 'common',
  filename: 'common.js'
});
const hmrPlugin = new webpack.HotModuleReplacementPlugin();

module.exports = {
  devtool: 'eval-source-map',

  context: path.resolve(__dirname, '..'),
  entry: {
    'js/app': './src/index.js'
  },
  output: {
    path: path.resolve(__dirname, '../public'),
    filename: '[name].bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: [ 'source-map-loader' ],
        enforce: 'pre'
      },
      {
        test: /\.scss$/,
        use:[{
          loader: 'style-loader'
        }, {
          loader: 'css-loader'
        }, {
          loader: 'sass-loader'
        }]
      },
      {
        test: /\.html$/,
        use: 'html-loader'
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        loaders: ['file-loader', 'image-webpack-loader']
      },
      {
        test: /\.(eot|woff|ttf)$/,
        use: [{
          loader: 'file-loader'
        }]
      }
    ]
  },

  devServer: {
    contentBase: path.join(__dirname, '../public'),
    compress: true,
    port: 8000,
    hot: true,
    historyApiFallback: true
  },

  plugins: [
    commonsChunk,
    htmlPlugin,
    hmrPlugin
  ]
};
