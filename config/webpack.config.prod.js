const path = require('path');
const webpack = require('webpack');

const config = require('./config');

const autoprefixer = require('autoprefixer');

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const uglifyJs = new UglifyJsPlugin({
  sourceMap: true
});
const extractSass = new ExtractTextPlugin({
  filename: '[name].css'
});
const htmlPlugin = new HtmlPlugin({
  template: './config/index.template.html',
  title: config.title,
  inject: 'body'
});
const commonsChunk = new webpack.optimize.CommonsChunkPlugin({
  name: 'common',
  filename: 'common.js'
});

module.exports = {
  devtool: 'source-map',

  context: path.resolve(__dirname, '..'),
  entry: {
    'js/app': './src/index.js'
  },
  output: {
    path: path.resolve(__dirname, '../build'),
    filename: '[name].bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: [ 'source-map-loader' ],
        enforce: 'pre'
      },
      {
        test: /\.scss$/,
        use: extractSass.extract({
          use: [{
            loader: 'css-loader'
          }, {
            loader: 'sass-loader'
          }, {
            loader: 'postcss-loader'
          }],
          fallback: 'style-loader'
        })
      },
      {
        test: /\.html$/,
        use: 'html-loader'
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: ['file-loader?publicPath=../&outputPath=images/', 'image-webpack-loader']
      },
      {
        test: /\.(eot|woff|ttf)$/,
        use: [{
          loader: 'file-loader'
        }]
      }
    ]
  },

  plugins: [
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false,
      options: {
        postcss: [
          autoprefixer()
        ]
      }
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    uglifyJs,
    extractSass,
    commonsChunk,
    htmlPlugin
  ]
};
