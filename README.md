# React with Typescript initial project #

## Install ##
`git clone git@gitlab.com:mdulowski/react.git . && npm install`

## Commands ##
* `npm start` - starts webpack-dev-server with hot module reloading
* `npm run build` - creates development build in __public/__
* `npm run build:prod` - creates production build in __build/__
* `npm run serve` - serves production assets from __build/__

## License ##
ISC
